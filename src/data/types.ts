interface Commerce {
  id: number;
  name: string;
  image: string;
}

interface Product {
  commerceId: number;
  id: number;
  name: string;
  price: number;
}

interface City {
  id: number;
  name: string;
}

export type { Commerce, Product, City };

import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import React, { ReactElement } from 'react';

import CommerceListView from '../views/commerce-list/commerce-list';
import NotFoundView from '../views/not-found/not-found';
import ProductListView from '../views/product-list/product-list';
import ShoppingCartView from '../views/shopping-cart/shopping-cart';

const IndexRouter: React.FC = (): ReactElement => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate replace to="/commerces" />} />
        <Route path="/commerces" element={<CommerceListView />} />
        <Route
          path="/commerces/:commerceId/products"
          element={<ProductListView />}
        />
        <Route path="/shopping-cart" element={<ShoppingCartView />} />
        <Route path="*" element={<NotFoundView />} />
      </Routes>
    </BrowserRouter>
  );
};

export default IndexRouter;

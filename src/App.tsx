import './App.css';

import React, { ReactElement } from 'react';

import IndexRouter from './routes';

const App: React.FC = (): ReactElement => {
  return <IndexRouter />;
};

export default App;

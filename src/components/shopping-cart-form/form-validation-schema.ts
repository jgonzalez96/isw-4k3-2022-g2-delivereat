import * as Yup from 'yup';

const formValidationSchema = Yup.object({
  city: Yup.string().required('Campo requerido'),
  street: Yup.string().required('Campo requerido'),
  streetNumber: Yup.number().required('Campo requerido'),
  reference: Yup.string().optional(),
  paymentMethod: Yup.string().required('Campo requerido'),
  cashValue: Yup.number().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'cash'
      ? schema.required('Campo requerido')
      : schema;
  }),
  // Visa :- Starting with 4, length 13 or 16 digits.
  // MasterCard :- Starting with 51 through 55, length 16 digits.
  cardNumber: Yup.string().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'visa'
      ? schema
          .required('Campo requerido')
          .matches(
            /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14}))$/,
            'Número de tarjeta inválido'
          )
      : schema;
  }),
  cardNames: Yup.string().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'visa'
      ? schema.required('Campo requerido')
      : schema;
  }),
  cardDueDate: Yup.string().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'visa'
      ? schema
          .required('Campo requerido')
          .matches(
            /^(0[1-9]|1[0-2])\/?([0-9]{4})$/,
            'Formato inválido (MM/YYYY)'
          )
          .test('test-name', 'La fecha debe ser futura', (value: string) => {
            if (!value) {
              return;
            }
            const values = value.split('/');
            const [month, year] = values.map((v) => parseInt(v));
            const dueDate = new Date(year, month, 1);
            if (dueDate < new Date()) {
              return false;
            }
            return true;
          })
      : schema;
  }),
  cardCvc: Yup.string().when('paymentMethod', (paymentMethod, schema) => {
    return paymentMethod === 'visa'
      ? schema
          .required('Campo requerido')
          .matches(/^\d{3}$/, 'Formato inválido')
      : schema;
  }),
  shipMethod: Yup.string().required('Campo requerido'),
  shipDate: Yup.date().when('shipMethod', (shipMethod, schema) => {
    return shipMethod === 'date'
      ? schema
          .required('Campo requerido')
          .min(new Date(), 'La fecha debe ser futura')
      : schema;
  }),
});

export default formValidationSchema;

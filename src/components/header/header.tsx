import './header.styles.css';

import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from '@mui/material';
import React, { ReactElement } from 'react';

import FastfoodIcon from '@mui/icons-material/Fastfood';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useNavigate } from 'react-router-dom';

const Header: React.FC = (): ReactElement => {
  const navigate = useNavigate();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            sx={{ mr: 2 }}
            onClick={() => navigate('/')}
          >
            <FastfoodIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            DeliverEat!
          </Typography>
          <Button
            size="large"
            color="inherit"
            startIcon={<ShoppingCartIcon />}
            onClick={() => navigate('/shopping-cart')}
          >
            Carrito
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;

import './commerce-list.styles.css';

import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from '@mui/material';
import React, { ReactElement } from 'react';

import commerces from '../../data/commerces.json';

const CommerceList: React.FC = (): ReactElement => {
  return (
    <Grid container spacing={6}>
      {commerces.map((commerce) => (
        <Grid item xs={12} sm={6} md={4} key={commerce.id}>
          <Card>
            <CardActionArea href={`/commerces/${commerce.id}/products`}>
              <CardMedia component="img" image={commerce.image} />
              <CardContent>
                <Typography variant="h5">{commerce.name}</Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default CommerceList;

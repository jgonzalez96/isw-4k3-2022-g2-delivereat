import './product-list.styles.css';

import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  IconButton,
  Typography,
} from '@mui/material';
import React, { ReactElement, useState } from 'react';

import AddIcon from '@mui/icons-material/Add';
import { Box } from '@mui/system';
import { Product } from '../../data/types';
import RemoveIcon from '@mui/icons-material/Remove';
import { addProduct } from '../../utils/local-storage';

const ProductItem: React.FC<{ product: Product }> = ({
  product,
}: {
  product: Product;
}): ReactElement => {
  const [quantity, setQuantity] = useState(1);

  const handleClickAdd = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    try {
      addProduct(product, quantity);
    } catch (err) {
      return alert(err);
    }
    alert('Producto añadido al carrito');
  };

  return (
    <Card variant="elevation">
      <CardContent>
        <Typography variant="h5">{product.name}</Typography>
        <Typography variant="body1">{`$${product.price}`}</Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: 'space-between' }}>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <IconButton
            color="inherit"
            onClick={() =>
              setQuantity((prevState) => (prevState > 1 ? prevState - 1 : 1))
            }
          >
            <RemoveIcon />
          </IconButton>
          <Typography variant="body1">{quantity}</Typography>
          <IconButton
            color="inherit"
            onClick={() => setQuantity((prevState) => prevState + 1)}
          >
            <AddIcon />
          </IconButton>
        </Box>

        <Button size="medium" onClick={handleClickAdd}>
          Añadir al carrito
        </Button>
      </CardActions>
    </Card>
  );
};

const ProductList: React.FC<{ products: Product[] }> = ({
  products,
}: {
  products: Product[];
}): ReactElement => {
  return (
    <Grid container spacing={6}>
      {products.map((product) => (
        <Grid
          item
          xs={12}
          sm={6}
          md={6}
          key={`${product.commerceId}${product.id}`}
        >
          <ProductItem product={product} />
        </Grid>
      ))}
    </Grid>
  );
};

export default ProductList;

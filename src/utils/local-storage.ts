import { Product } from '../data/types';

interface Item {
  quantity: number;
  product: Product;
}

const addProduct = (product: Product, quantity: number): void => {
  // Validate total products
  if (quantity > 5) {
    throw new Error('No se puede agregar más de 5 productos al carrito');
  }

  const products = localStorage.getItem('products');
  const item: Item = {
    quantity,
    product,
  };

  if (!products) {
    localStorage.setItem('products', JSON.stringify([item]));
    return;
  }

  const parsedProducts: Item[] = JSON.parse(products);

  // Validate addition
  if (
    parsedProducts.some(
      (parsedProduct) => parsedProduct.product.commerceId !== product.commerceId
    )
  ) {
    throw new Error(
      'No se puede agregar al carrito productos de distintos comercios'
    );
  }

  if (
    parsedProducts.find(
      (parsedProduct) => parsedProduct.product.id === product.id
    )
  ) {
    throw new Error('El producto ya existe en el carrito');
  }

  parsedProducts.push(item);

  // Validate total products
  let numProducts = 0;
  parsedProducts.forEach((item) => (numProducts += item.quantity));
  if (numProducts > 5) {
    throw new Error('No se puede agregar más de 5 productos al carrito');
  }

  localStorage.setItem('products', JSON.stringify(parsedProducts));
};

const getProducts = (): Item[] => {
  const products = localStorage.getItem('products') || '[]';
  return JSON.parse(products);
};

const removeProduct = (id: number): void => {
  const products = localStorage.getItem('products');
  if (!products) {
    return;
  }
  let parsedProducts: Item[] = JSON.parse(products);
  parsedProducts = parsedProducts.filter(
    (product) => product.product.id !== id
  );
  localStorage.setItem('products', JSON.stringify(parsedProducts));
};

const setProduct = (item: Item) => {
  removeProduct(item.product.id);
  addProduct(item.product, item.quantity);
};

const removeProducts = () => {
  localStorage.setItem('products', JSON.stringify([]));
};

export { addProduct, getProducts, removeProduct, setProduct, removeProducts };
export type { Item };
